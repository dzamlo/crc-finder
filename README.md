# crc-finder

To use this library:
```
python -m venv .env
source .env/bin/activate
pip install maturin
maturin develop --release
```

Then in a python shell/script:
```
import crc_finder
crc_finder.find_crc_32([(b"123456789", b"abcd"), (b"123456789", b"abcd")])
```

## `CrcComputeMethod` and performance

The library we use to compute the crcs (thanks to the authors of `crcxx`) provide multiple choice for the method to compute them. These methods have different trade-off between the initialization time and the speed to compute the crcs. Depending on the length of your message, the ideal trade-off will be different.

# Thanks

Thanks to Gregory Ewing for his article ["Reverse-Engineering a CRC Algorithm"](https://www.csse.canterbury.ac.nz/greg.ewing/essays/CRC-Reverse-Engineering.html) and Colin O'Flynn for his work on [crcbeagle](https://github.com/colinoflynn/crcbeagle).

# License

Licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be dual licensed as above, without any additional terms or
conditions.