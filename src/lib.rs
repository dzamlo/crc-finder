use std::ops::Range;
use std::sync::Mutex;

use crcxx::crc32::*;
use crcxx::Params;
use indicatif::ParallelProgressIterator;
use indicatif::ProgressStyle;
use pyo3::exceptions::PyOSError;
use pyo3::prelude::*;
use rayon::prelude::*;
use thiserror::Error;

// For optimal performance, you need to adjust this type. The optimal one
// depends on the length of your messages.
type CrcComputeMethod = LookupTable256xN<8>;

#[derive(Error, Debug)]
pub enum FindCrcError {
    #[error("not at least two messages")]
    NotAtLeastTwoMessages,
    #[error("not all messages share the same length")]
    NotAllMessagesSameLength,
    #[error("index is not smaller than count")]
    IndexNotSmallerThanCount,
}

impl std::convert::From<FindCrcError> for PyErr {
    fn from(err: FindCrcError) -> PyErr {
        PyOSError::new_err(err.to_string())
    }
}

#[pyclass]
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, PartialOrd, Ord)]
pub enum Endianness {
    Little,
    Big,
}

#[pyclass(get_all, set_all)]
#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq, PartialOrd, Ord)]
pub struct CrcConfig {
    poly: u32,
    xor_out: u32,
    refin: bool,
    refout: bool,
    endianness: Endianness,
}

fn get_crc_vec<const CRC_LENGTH: usize>(
    digest: u32,
    endianness: Endianness,
    refout: bool,
) -> [u8; CRC_LENGTH] {
    let digest = if !refout {
        digest.reverse_bits() >> (32 - CRC_LENGTH * 8)
    } else {
        digest
    };
    if endianness == Endianness::Big {
        digest.to_be_bytes()[(4 - CRC_LENGTH)..].try_into().unwrap()
    } else {
        digest.to_le_bytes()[..CRC_LENGTH].try_into().unwrap()
    }
}

#[allow(clippy::too_many_arguments)]
fn check_crc<const CRC_LENGTH: usize>(
    message1: &[u8],
    crc1: [u8; CRC_LENGTH],
    message2: &[u8],
    crc2: [u8; CRC_LENGTH],
    crcs_xored: [u8; CRC_LENGTH],
    poly: u32,
    refin: bool,
    refout: bool,
    endianness: Endianness,
    crc_calculator: &Crc<CrcComputeMethod>,
    digest: u32,
) -> Option<CrcConfig> {
    let crc_vec = get_crc_vec(digest, endianness, refout);

    if crc_vec == crcs_xored {
        let digest = crc_calculator.compute(message1);
        let new_crc: [_; CRC_LENGTH] = get_crc_vec(digest, endianness, refout);
        let mut xor_out1_array = [0u8; CRC_LENGTH];
        for i in 0..CRC_LENGTH {
            xor_out1_array[i] = new_crc[i] ^ crc1[i];
        }

        let digest = crc_calculator.compute(message2);
        let new_crc: [_; CRC_LENGTH] = get_crc_vec(digest, endianness, refout);

        let mut xor_out2_array = [0u8; CRC_LENGTH];
        for i in 0..CRC_LENGTH {
            xor_out2_array[i] = new_crc[i] ^ crc2[i];
        }

        if xor_out1_array == xor_out2_array {
            let mut xor_out = 0u32;
            if endianness == Endianness::Little {
                xor_out1_array.reverse();
            }
            for x in xor_out1_array {
                xor_out <<= 8;
                xor_out |= x as u32;
            }

            Some(CrcConfig {
                poly,
                xor_out,
                refin,
                refout,
                endianness,
            })
        } else {
            None
        }
    } else {
        None
    }
}

fn make_range(start: u32, end: u32, index: u32, count: u32) -> Range<u32> {
    let pivot = (end - start) / count + start;
    if index == 0 {
        start..pivot
    } else {
        make_range(pivot, end, index - 1, count - 1)
    }
}

fn find_crc_single_couple<const CRC_LENGTH: usize>(
    message1: &[u8],
    crc1: [u8; CRC_LENGTH],
    message2: &[u8],
    crc2: [u8; CRC_LENGTH],
    index: u32,
    count: u32,
) -> Vec<CrcConfig> {
    let r = Mutex::new(Vec::new());

    let messages_xored: Vec<u8> = message1.iter().zip(message2).map(|(a, b)| a ^ b).collect();
    let mut crcs_xored = [0u8; CRC_LENGTH];
    for i in 0..CRC_LENGTH {
        crcs_xored[i] = crc1[i] ^ crc2[i];
    }

    let style = ProgressStyle::with_template(
        "[{elapsed_precise}] {bar:40.cyan/blue} {pos:>10}/{len:10}, {eta} remaining",
    )
    .unwrap();

    (make_range(0, 1 << (CRC_LENGTH * 8 - 1), index, count))
        .into_par_iter()
        .progress_with_style(style)
        .for_each(|poly_high| {
            let poly = (poly_high << 1) | 1;
            for refin in [true, false] {
                let algo = Params {
                    width: CRC_LENGTH as u8 * 8,
                    poly,
                    init: 0,
                    refin,
                    refout: true,
                    xorout: 0,
                    check: 0,
                    residue: 0,
                };
                {
                    let crc = Crc::<CrcComputeMethod>::new(&algo);
                    let digest = crc.compute(&messages_xored);

                    for refout in [true, false] {
                        for endianness in [Endianness::Big, Endianness::Little] {
                            if let Some(config) = check_crc(
                                message1, crc1, message2, crc2, crcs_xored, poly, refin, refout,
                                endianness, &crc, digest,
                            ) {
                                println!("A config was found: {config:?}");
                                r.lock().unwrap().push(config);
                            }
                        }
                    }
                }
            }
        });

    r.into_inner().unwrap()
}

fn compute_crc<const CRC_LENGTH: usize>(config: &CrcConfig, message: &[u8]) -> [u8; CRC_LENGTH] {
    let algo = Params {
        width: CRC_LENGTH as u8 * 8,
        poly: config.poly,
        init: 0,
        refin: config.refin,
        refout: config.refout,
        xorout: config.xor_out,
        check: 0,
        residue: 0,
    };
    let crc_calculator = Crc::<CrcComputeMethod>::new(&algo);
    let digest = crc_calculator.compute(message);
    get_crc_vec(digest, config.endianness, true)
}

fn validate_crc_config<const CRC_LENGTH: usize>(
    config: &CrcConfig,
    message: &[u8],
    crc: [u8; CRC_LENGTH],
) -> bool {
    compute_crc(config, message) == crc
}

fn find_crc<const CRC_LENGTH: usize>(
    messages_with_crcs: Vec<(&[u8], [u8; CRC_LENGTH])>,
    index: u32,
    count: u32,
) -> Result<Vec<CrcConfig>, FindCrcError> {
    if messages_with_crcs.len() < 2 {
        return Err(FindCrcError::NotAtLeastTwoMessages);
    }

    let messages_length = messages_with_crcs[0].0.len();
    if messages_with_crcs
        .iter()
        .map(|(m, _)| m.len())
        .any(|l| l != messages_length)
    {
        return Err(FindCrcError::NotAllMessagesSameLength);
    }

    if index >= count {
        return Err(FindCrcError::IndexNotSmallerThanCount);
    }

    println!("searching with message 0 and the next");
    let message1 = messages_with_crcs[0].0;
    let crc1 = messages_with_crcs[0].1;
    let message2 = messages_with_crcs[1].0;
    let crc2 = messages_with_crcs[1].1;
    let mut result = find_crc_single_couple(message1, crc1, message2, crc2, index, count);
    let result_len = result.len();
    println!("found {result_len} crc configs");

    #[allow(clippy::needless_range_loop)]
    for i in 2..messages_with_crcs.len() {
        if result.is_empty() {
            break;
        }
        println!("Checking configs with message {i}");
        let (message, crc) = messages_with_crcs[i];
        result.retain(|config| validate_crc_config(config, message, crc));
        let configs_len = result.len();
        println!("{configs_len} configs remaining");
    }

    Ok(result)
}

#[pyfunction]
#[pyo3(signature=(messages_with_crcs, index=0, count=1))]
pub fn find_crc_8(
    messages_with_crcs: Vec<(&[u8], [u8; 1])>,
    index: u32,
    count: u32,
) -> Result<Vec<CrcConfig>, FindCrcError> {
    find_crc(messages_with_crcs, index, count)
}

#[pyfunction]
#[pyo3(signature=(messages_with_crcs, index=0, count=1))]
pub fn find_crc_16(
    messages_with_crcs: Vec<(&[u8], [u8; 2])>,
    index: u32,
    count: u32,
) -> Result<Vec<CrcConfig>, FindCrcError> {
    find_crc(messages_with_crcs, index, count)
}

#[pyfunction]
#[pyo3(signature=(messages_with_crcs, index=0, count=1))]
pub fn find_crc_24(
    messages_with_crcs: Vec<(&[u8], [u8; 3])>,
    index: u32,
    count: u32,
) -> Result<Vec<CrcConfig>, FindCrcError> {
    find_crc(messages_with_crcs, index, count)
}

#[pyfunction]
#[pyo3(signature=(messages_with_crcs, index=0, count=1))]
pub fn find_crc_32(
    messages_with_crcs: Vec<(&[u8], [u8; 4])>,
    index: u32,
    count: u32,
) -> Result<Vec<CrcConfig>, FindCrcError> {
    find_crc(messages_with_crcs, index, count)
}

#[pyfunction]
pub fn compute_crc_8(config: &CrcConfig, message: &[u8]) -> [u8; 1] {
    compute_crc(config, message)
}

#[pyfunction]
pub fn compute_crc_16(config: &CrcConfig, message: &[u8]) -> [u8; 2] {
    compute_crc(config, message)
}

#[pyfunction]
pub fn compute_crc_24(config: &CrcConfig, message: &[u8]) -> [u8; 3] {
    compute_crc(config, message)
}
#[pyfunction]
pub fn compute_crc_32(config: &CrcConfig, message: &[u8]) -> [u8; 4] {
    compute_crc(config, message)
}

#[pymodule]
fn crc_finder(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<Endianness>()?;
    m.add_class::<CrcConfig>()?;
    m.add_function(wrap_pyfunction!(find_crc_8, m)?)?;
    m.add_function(wrap_pyfunction!(find_crc_16, m)?)?;
    m.add_function(wrap_pyfunction!(find_crc_24, m)?)?;
    m.add_function(wrap_pyfunction!(find_crc_32, m)?)?;
    m.add_function(wrap_pyfunction!(compute_crc_8, m)?)?;
    m.add_function(wrap_pyfunction!(compute_crc_16, m)?)?;
    m.add_function(wrap_pyfunction!(compute_crc_24, m)?)?;
    m.add_function(wrap_pyfunction!(compute_crc_32, m)?)?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use crate::make_range;

    #[test]
    fn make_range_works() {
        assert_eq!(make_range(0, 10, 0, 2), 0..5);
        assert_eq!(make_range(0, 10, 1, 2), 5..10);

        assert_eq!(make_range(0, 20, 0, 3), 0..6);
        assert_eq!(make_range(0, 20, 1, 3), 6..13);
        assert_eq!(make_range(0, 20, 2, 3), 13..20);
    }
}
